package com.company;

/**
 * This class is responsible
 * of keeping track of teacher´s name, ID and salary
 */

public class Teacher {
    private int id;
    private String name;
    private int salary;
    private int totalSalaryPayment;

    public Teacher(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public int getTotalSalaryPayment() {
        return totalSalaryPayment;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    //Not going to later teacher´s ID and name

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void paySalary() {
        this.totalSalaryPayment += salary;
        School.updateTotalMoneySpent(salary);
        System.out.println(name + " got paid " + salary + "€");
    }
}
