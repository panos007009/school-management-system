package com.company;

/**
  This  class is responsible of keeping the record of
  the list of teachers and students of the school,
  total incomes, expenses and money left.
*/

import java.util.List;

public class School {

    public static Object updateTotalMoneyEarned;
    private List<Teacher> teachers;
    private List<Student> students;
    private static int totalMoneyEarned;
    private static int totalMoneySpent;
    private static int totalMoneyLeft;

    public School(List<Teacher> teachers, List<Student> students) {
        this.teachers = teachers;
        this.students = students;
        this.totalMoneyEarned = 0;
        this.totalMoneySpent = 0;
        this.totalMoneyLeft= 0;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void addTeacher (Teacher teacherToAdd) {
        teachers.add(teacherToAdd);
    }

    public void removeTeacher (Teacher teacherToRemove) {
        teachers.remove(teacherToRemove);
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student studentToAdd) {
        students.add(studentToAdd);
    }

    public void removeStudent(Student studentToRemove) {
        students.remove(studentToRemove);
    }

    public int getTotalMoneyEarned() {
        return totalMoneyEarned;
    }

    public static void updateTotalMoneyEarned(int moneyEarned) {
        totalMoneyEarned += moneyEarned;
        updateTotalMoneyLeft();
    }

    public int getTotalMoneySpent() {
        return totalMoneySpent;
    }

    public static void updateTotalMoneySpent(int moneySpent) {
        totalMoneySpent +=  moneySpent;
        updateTotalMoneyLeft();
    }

    public int getTotalMoneyLeft() {
        return totalMoneyLeft;
    }

    private static void updateTotalMoneyLeft() {
        totalMoneyLeft = totalMoneyEarned - totalMoneySpent;
    }
}
