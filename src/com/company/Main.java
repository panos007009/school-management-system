package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	Teacher Lizzy = new Teacher(1,"Lizzy",500);
	Teacher Erica = new Teacher(2,"Erica", 700);
	Teacher Alex = new Teacher(3,"Alex", 700);

	List<Teacher> teachersList = new ArrayList<>();
	teachersList.add(Lizzy);
	teachersList.add(Erica);
	teachersList.add(Alex);

	Student Alessia = new Student(1,"Alessia",4);
	Student Mark = new Student(2,"Mark", 7);

	List<Student> studentsList = new ArrayList<>();
	studentsList.add(Alessia);
	studentsList.add(Mark);

	School MIT = new School(teachersList,studentsList);
	Alessia.payFees(5000);
	Mark.payFees(4000);
    System.out.println("MIT earned : " + MIT.getTotalMoneyEarned() + "€");
	Lizzy.paySalary();
	Erica.paySalary();
	System.out.println("MIT has spent : " + MIT.getTotalMoneySpent() + "€");
	System.out.println("MIT has " + MIT.getTotalMoneyLeft() + "€ in the bank");
		for (Student student : studentsList) {
			System.out.println("ID# : " + student.getId() + "  NAME : " + student.getName());
		}

    }
}
