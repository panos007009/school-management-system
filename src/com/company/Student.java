package com.company;

/**
 * This class keeps track of
 * student  names, fees/fees paid and grades
 */

public class Student {

    private int id;
    private String name;
    private double grade;
    private int feesPaid;
    private int fees;

    public Student(int id, String name, double grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.feesPaid = 0;
        this.fees = 30000;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getGrade() {
        return grade;
    }

    public int getFeesPaid() {
        return feesPaid;
    }

    public int getFees() {
        return fees;
    }

    //Not going to alter student´s name, student´s ID.


    public void setGrade(double grade) {
        if (grade>0 && grade <= 10)
        this.grade = grade;
    }

    public void payFees(int paidAmount) {
        feesPaid += paidAmount;
        School.updateTotalMoneyEarned(paidAmount);
        System.out.println(name + " paid " + paidAmount +"€");

    }

    public int getRemainingFees () {
        return fees - feesPaid;
    }

}
